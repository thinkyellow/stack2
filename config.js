module.exports = function (...configs) {
  var config = {}
  for (var i = 0; i < configs.length; i++) {
    configs[i] = configs[i].call(config)
  }
  config = Object.assign(config, ...configs)

  var resolve = function (_config) {
    var keys = Object.keys(_config)
    for (var i = 0; i < keys.length; i++) {
      var curKey = keys[i]
      var curVal = _config[keys[i]]
      if (typeof curVal === 'function') {
        _config[curKey] = curVal.call(this)
      } else if (typeof curVal === 'object') {
        _config[curKey] = resolve.call(this, curVal)
      }
    }
    return _config
  }

  return resolve.call(config, config)
}
