module.exports = {
  plugins: [
    require('postcss-will-change-transition'),
    require('postcss-calc'),
    require('postcss-hocus'),
    require('autoprefixer')({
      grid: true,
      browsers: ['> 0%']
    })/*,
    require('cssnano')({
      autoprefixer: false
    })*/
  ]
};
