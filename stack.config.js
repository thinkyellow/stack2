module.exports = () => ({
  sourceFolder: '../src/',
  source: {
    scss () {
      return this.sourceFolder + 'scss/'
    },
    js () {
      return this.sourceFolder + 'js/'
    },
    node () {
      return this.sourceFolder + 'js/node_modules/'
    },
    packageJSONParent () {
      return this.source.js
    },
    packageJSON () {
      return this.source.js + '/package.json'
    },
    img () {
      return this.sourceFolder + 'img/'
    },
    fonts () {
      return this.sourceFolder + 'fonts/'
    }
  },
  sourcePaths: {
    scss () {
      return this.source.scss + '**/*.scss'
    },
    js () {
      return this.source.js + '**/[^_]*.js'
    },
    excludeNode () {
      return '!' + this.source.node + '**/*.*'
    },
    excludeVendor () {
      return '!' + this.source.js + 'vendor/**/*.*'
    },
    vendor () {
      return this.source.js + 'vendor/**/*.*'
    },
    img () {
      return this.source.img + '**/*.{jpg,png,gif,svg}'
    },
    fonts () {
      return this.source.fonts + '**/*.*'
    }
  },
  destinationFolder: '../public/assets/',
  destination: {
    css () {
      return this.destinationFolder + 'css/'
    },
    js () {
      return this.destinationFolder + 'js/'
    },
    img () {
      return this.destinationFolder + 'img/'
    },
    vendor () {
      return this.destination.js + 'vendor/'
    },
    fonts () {
      return this.destinationFolder + 'fonts/'
    }
  }
})
