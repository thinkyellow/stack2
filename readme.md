# Smidswater Stack 2

## IF any issues occur with running the stack
```
Make sure youre using node 10. Also, check the following claude thread: https://claude.ai/chat/37504328-9b28-4021-9ec0-a6bd15dbae2d
```

## Add git module
```
git submodule add git@bitbucket.org:thinkyellow/stack2.git stack2
```

## Update git module
```
git submodule update --init
```

## Install stack2
```
cd stack2
npm i
```

## Edit config (parent folder of stack2)
```
nano stack.config.js
```

## General tasks

### Build
```
npm run build
```

### Watch
```
npm run watch
```
