/* global process */
const defaultConfig = require('./stack.config');
var externalConfig = () => ({});
try {
  externalConfig = require('../stack.config');
} catch (e) {
  externalConfig = () => ({});
}
const config = require('./config')(defaultConfig, externalConfig);
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const fs = require('fs-extra');
const postcss = require('gulp-postcss');
const gulpImageMin = require('gulp-imagemin');
const imageMinPNGQuant = require('imagemin-pngquant');
const gulpRun = require('gulp-run');
const gulpChanged = require('gulp-changed');
const gulpCacheSass = require('gulp-cached-sass');
const gulpIgnore = require('gulp-ignore');
const babelify = require('babelify');

gulp.task('process:scss', () => gulp.src(config.sourcePaths.scss)
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(postcss({
    config: process.cwd() + '/css/postcss.config.js'
  }))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(config.destination.css))
);

gulp.task('process:img', () => gulp.src(config.sourcePaths.img)
  .pipe(gulpChanged(config.destination.img))
  .pipe(gulpImageMin({
    progressive: true,
    svgoPlugins: [{
      removeViewBox: false
    }],
    use: [imageMinPNGQuant()]
  }))
  .pipe(gulp.dest(config.destination.img))
);

function symLink(source, dest) {
  try {
    fs.readlinkSync(dest);
  } catch (e) {
    fs.symlinkSync(source, dest);
  }
}

gulp.task('init', (done) => {
  symLink('stack2/.sass-lint.yml', '../.sass-lint.yml');
  symLink('stack2/.eslintrc.js', '../.eslintrc.js');
  symLink('stack2/.editorconfig', '../.editorconfig');

  if (!fs.existsSync('../stack.config.js')) {
    fs.copySync('stack.config.js', '../stack.config.js');
  }
  var packageJSONExists = fs.existsSync(config.source.packageJSON);
  if (!packageJSONExists) {
    fs.copySync('./_package.json', config.source.packageJSON);
  }
  var cmd = new gulpRun.Command('npm install', {
    cwd: config.source.packageJSONParent
  });
  cmd.exec();
  done();

});
var browserify = require('persistify');
var through2 = require('through2').obj;
gulp.task('process:js', () => {
  var browserified = through2(function (file, enc, next) {
    var b = browserify(Object.assign({}, {
      entries: file.path,
      debug: true
    }), {
      watch: false
    });

    b.plugin(require.resolve('common-shakeify'));
    b.transform(babelify.configure({
      presets: [
        [require.resolve('@babel/preset-env'), {
          targets: {
            browsers: ['IE 10', '> 1%', 'last 2 versions']
          }
        }],
        require.resolve('@babel/preset-flow')
      ],
      plugins: [
        require.resolve('@babel/plugin-transform-runtime'),
        // Stage 0
        require.resolve('@babel/plugin-proposal-function-bind'),

        // Stage 1
        require.resolve('@babel/plugin-proposal-export-default-from'),
        require.resolve('@babel/plugin-proposal-logical-assignment-operators'),
        [require.resolve('@babel/plugin-proposal-optional-chaining'), {
          'loose': false
        }],
        [require.resolve('@babel/plugin-proposal-pipeline-operator'), {
          'proposal': 'minimal'
        }],
        [require.resolve('@babel/plugin-proposal-nullish-coalescing-operator'), {
          'loose': false
        }],
        require.resolve('@babel/plugin-proposal-do-expressions'),
        // Stage 2
        [require.resolve('@babel/plugin-proposal-decorators'), {
          'legacy': true
        }],
        require.resolve('@babel/plugin-proposal-function-sent'),
        require.resolve('@babel/plugin-proposal-export-namespace-from'),
        require.resolve('@babel/plugin-proposal-numeric-separator'),
        require.resolve('@babel/plugin-proposal-throw-expressions'),

        // Stage 3
        require.resolve('@babel/plugin-syntax-dynamic-import'),
        require.resolve('@babel/plugin-syntax-import-meta'),
        [require.resolve('@babel/plugin-proposal-class-properties'), {
          'loose': false
        }],
        require.resolve('@babel/plugin-proposal-json-strings'),
        require.resolve('@babel/plugin-proposal-object-rest-spread'),
        [require.resolve('babel-plugin-console-source'), {
          'fullPath': false
        }],
        [require.resolve('babel-plugin-holes'), {}]
      ]
    }), {
      sourceMaps: true
    });
    b.transform(require.resolve('browserify-cdnjs'));
    b.transform(require.resolve('uglifyify'), {
      global: true
    });
    file.contents = b.bundle();
    this.push(file);
    next();
  });
  return gulp.src(config.sourcePaths.js, {
    read: false,
    base: config.source.js,
    taskName: 'process:js'
  })
    .pipe(gulpIgnore.exclude(config.sourcePaths.excludeNode.substr(1 + config.source.js.length)))
    .pipe(gulpIgnore.exclude(config.sourcePaths.excludeVendor.substr(1 + config.source.js.length)))
    .pipe(browserified)
    .pipe(gulp.dest(config.destination.js));
});
gulp.task('copy:vendorJS', () => gulp.src(config.sourcePaths.vendor)
  .pipe(gulp.dest(config.destination.vendor)));

gulp.task('copy:fonts', () => gulp.src(config.sourcePaths.fonts)
  .pipe(gulp.dest(config.destination.fonts)));

gulp.task('watch:scss', () => {
  gulp.watch(config.sourcePaths.scss, gulp.parallel('process:scss'));
});
gulp.task('watch:fonts', () => {
  gulp.watch(config.sourcePaths.fonts, gulp.parallel('copy:fonts'));
});
gulp.task('watch:img', () => {
  gulp.watch(config.sourcePaths.img, gulp.parallel('process:img'));
});
gulp.task('watch:js', () => {
  gulp.watch([config.sourcePaths.js, config.sourcePaths.excludeNode, config.sourcePaths.excludeVendor], gulp.parallel('process:js'));
  gulp.watch(config.sourcePaths.vendor, gulp.parallel('copy:vendorJS'));
});
gulp.task('build', gulp.parallel('copy:fonts', 'copy:vendorJS', 'process:scss', 'process:js', 'process:img'));
gulp.task('watch', gulp.parallel('watch:scss', 'watch:js', 'watch:img', 'watch:fonts', 'build'));
gulp.task('default', gulp.series('watch'));
